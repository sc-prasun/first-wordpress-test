


jQuery(function($){


var getclone = $('.logo-section').clone();
$('.nav-sup').prepend(getclone);

$('.nav-sup').prepend('<span class="nav-cross"></span>');

var mobilenemufn = function(){
    $('.nav-sup').addClass('active-nav');
    $('body').prepend('<span class="active-overlay"></span>');
}
$('.mobilemenu').click(mobilenemufn);
var overlay = function(){
    $('.active-overlay').remove();
    $('.nav-sup').removeClass('active-nav');
}
$('body').on('click','.active-overlay',overlay)

$('.nav-sup').on('click', '.nav-cross',overlay)




var partners = function(){
 if($('.banner-item').length > 1){ 
 $(".main-banner-fld").owlCarousel({
        navigation: true,
        loop: true,
        margin: 0,
        navText: ["", ""],
        nav: true,
        autoplay:  true,
        mouseDrag: true,
        touchDrag: true,
        paginationSpeed: 1000,
        lazyLoad: false,
        autoplayTimeout: 4000,
        autoplayHoverPause: false,
        smartSpeed: 1000,
        navSpeed: 1000,
        dotsSpeed: 1000,
        autoPlaySpeed: 1000,
        autoHeight: false,
        responsive: {
            0: {
                items: 1
            },
            640: {
                items: 1
            },
            769: {
                items: 1
            }
        },
    });
 }
}
partners();





var product = function(){
 if($('.product-item').length > 1){ 
 $(".product-list-item").owlCarousel({
        navigation: true,
        loop: true,
        margin: 25,
        navText: ["", ""],
        nav: true,
        autoplay:  false,
        mouseDrag: true,
        touchDrag: true,
        paginationSpeed: 1000,
        lazyLoad: false,
        autoplayTimeout: 4000,
        autoplayHoverPause: false,
        smartSpeed: 1000,
        navSpeed: 1000,
        dotsSpeed: 1000,
        autoPlaySpeed: 1000,
        autoHeight: false,
        responsive: {
            0: {
                items: 1
            },
            640: {
                items: 1
            },
            701: {
                items: 3
            },
            768: {
                items: 4
            }
        },
    });
 }
}
product();





var partners = function(){
 if($('.partners-item').length > 1){ 
 $(".partners-slide-section").owlCarousel({
        navigation: true,
        loop: true,
        margin: 25,
        navText: ["", ""],
        nav: true,
        autoplay:  false,
        mouseDrag: true,
        touchDrag: true,
        paginationSpeed: 1000,
        lazyLoad: false,
        autoplayTimeout: 4000,
        autoplayHoverPause: false,
        smartSpeed: 1000,
        navSpeed: 1000,
        dotsSpeed: 1000,
        autoPlaySpeed: 1000,
        autoHeight: false,
        responsive: {
            0: {
                items: 2
            },
            640: {
                items: 3
            },
            769: {
                items: 6
            }
        },
    });
 }
}
partners();





var search = function(){
  $('.search-form').stop(true,true).fadeToggle();
  $(this).toggleClass('active-search');
}
$('.srch').click(search);

$('.horizontalTab').easyResponsiveTabs({
  type: 'horizontal',
  width: 'auto',
  fit: true,
  activate: function (event, ui) {
         window.dispatchEvent(new Event('resize'));
     }
});


function accordionfunction(m,n){
      $(this).find('ul').parent('li').append('<i class="fa fa-angle-down nav-arrow"></i>');
      function accrdeach(p,q){
         if($(q).hasClass('active')){
            var $prnt = $(this).parents('li');
                $prnt.addClass('active-arrow');
                $prnt.find('ul').slideDown();
         }
    }
       $(n).find('ul li').each(accrdeach);
      function accordion(){
            var $getparent = $(this).parents('li');
             $(this).prev('ul').stop(true,true).slideToggle(400);
             $getparent.toggleClass('active-arrow');
             $getparent.siblings('li').find('ul').slideUp(400).end().removeClass('active-arrow');
      }
      $(n).on('click','.nav-arrow', accordion)
    }
$('.sidebar-category >  ul > li').each(accordionfunction);



function searchani(){
     $('.search-form').addClass('active-search-form');
     $('body').prepend('<span class="search-fixed-overlay"></span>');
     $('.search-fixed-overlay').delay(100).queue(function(){
        $(this).addClass('active-search-fixed');
     });
     $('html').css('overflow', 'hidden');
}
$('.srch').click(searchani);
$('.search-form').prepend('<i class="fa fa-times iclose"></i>');

function searchclose(){
    var $getsecrch = $(this).parent();
        $getsecrch.removeClass('active-search-form');
        $getsecrch.fadeOut(600);
        $('.search-fixed-overlay').remove();
        $('html').css('overflow', 'visible');
}
  $('.search-form').on('click','.iclose',searchclose);

$('body').on('click','.search-fixed-overlay', function(){
   $('.search-form').removeClass('active-search-form');
   $('.search-form').fadeOut(600);
   $('.search-fixed-overlay').remove();
   $('html').css('overflow', 'visible');
});


var getheight = $('.main-products-section').innerHeight();

$(window).on('on resize', function(){
 $('.main-products-section').css('height',getheight);
});




$('#product_image .simpleLens-thumbnails-container img').simpleGallery({
    loading_image: 'images/loading.gif'
});

$('#product_image .simpleLens-big-image').simpleLens({
    loading_image: 'images/loading.gif'
});

$('.simpleLens-thumbnail-wrapper').click(function(e){
    e.preventDefault();
})



$('.thumb_slider').slick({
  dots: false,
  infinite: true,
  speed:1000,
  autoplay:false,
  pauseOnHover:false,
  arrows:true,
  vertical:true,
  slidesToShow: 3,
  responsive: [
    {
      breakpoint: 521,
      settings: {
        slidesToShow: 2
      }
    },
    ]
});


$('.open-popup-link').magnificPopup({
  type:'inline',
  midClick: true
});


});




$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});








    


	
	
	
	
	
	
	
	
	
	
	
	